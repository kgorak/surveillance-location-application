# build backend
FROM golang:1.14-alpine AS backend-builder

RUN mkdir -p /opt/app
WORKDIR /opt/app

COPY ./backend/go.mod ./backend/go.sum ./
RUN CGO_ENABLED=0 GOOS=linux go mod download

COPY ./backend/ ./

RUN CGO_ENABLED=0 GOOS=linux go build

# build frontend
FROM node:14-alpine AS frontend-builder

RUN mkdir -p /opt/app
WORKDIR /opt/app

COPY ./frontend/package.json ./package.json
RUN yarn install

COPY ./frontend/ ./
RUN yarn build

# final image
FROM scratch

EXPOSE 3000

COPY ./backend/.env ./.env
COPY --from=backend-builder /opt/app/locator ./locator
COPY --from=frontend-builder /opt/app/dist ./dist

ENTRYPOINT ["./locator"]
