#!/usr/bin/env sh

chokidar --verbose -d 500 -t 200 -i '**/.#*' -c "go build && ./locator" '**/*.go'
