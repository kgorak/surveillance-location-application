package controllers

import (
	"fmt"
	"locator/models"
	"locator/utils"
	"locator/utils/tokens"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
)

// UserDeviceController represents controller for devices associated with the logged user.
type UserDeviceController struct {
	DeviceStore   models.IDeviceStore
	LocationStore models.ILocationStore
}

// FindAll returns all devices associated with the logged user as a PageableResponse.
func (t *UserDeviceController) FindAll(c echo.Context) error {
	db := utils.GetDB(c)
	user := utils.GetUser(c)
	pageable := utils.GetPageable(c)

	devices, totalElements, err := t.DeviceStore.FindByUserId(db, user.ID, pageable.Page, pageable.Size)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, &models.PageableResponse{
		Page:          pageable.Page,
		Size:          pageable.Size,
		TotalElements: totalElements,
		Data:          &devices,
	})
}

// Create adds a device associated with the logged user.
func (t *UserDeviceController) Create(c echo.Context) error {
	db := utils.GetDB(c)
	user := utils.GetUser(c)

	var input models.CreateDeviceForm
	if err := BindAndValidate(c, &input); err != nil {
		return err
	}

	var name string
	if input.Name != nil {
		name = *input.Name
	} else {
		name = "Urządzenie dodane " + time.Now().Format("02-01-2006")
	}

	key := tokens.CreateDeviceAPIKey()

	device, err := t.DeviceStore.Create(db, user.ID, name, key)

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, &device)
}

// Get returns a device by its id if it belongs to the logged user.
func (t *UserDeviceController) Get(c echo.Context) error {
	db := utils.GetDB(c)
	user := utils.GetUser(c)
	id, err := utils.GetIdParam(c)
	if err != nil {
		return err
	}

	device, err := t.DeviceStore.GetById(db, id)
	if err != nil {
		return err
	}

	if device.UserID != user.ID {
		return c.NoContent(http.StatusNotFound) // TODO: maybe change to 403
	}

	return c.JSON(http.StatusOK, &device)
}

// Delete removes a device by its id if it belongs to the logged user.
func (t *UserDeviceController) Delete(c echo.Context) error {
	db := utils.GetDB(c)
	user := utils.GetUser(c)
	id, err := utils.GetIdParam(c)
	if err != nil {
		return err
	}

	err = t.DeviceStore.DeleteByIdAndUserId(db, id, user.ID)

	if err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

// FindAllLocations returns all locations associated with a device if the device belongs to the logged user.
func (t *UserDeviceController) FindAllLocations(c echo.Context) error {
	var input models.QueryLocationForm
	if err := BindAndValidate(c, &input); err != nil {
		return err
	}

	fmt.Println("QueryLocationForm", input.After, input.Before)

	user := utils.GetUser(c)
	db := utils.GetDB(c)
	deviceId, err := utils.GetIdParam(c)
	if err != nil {
		return err
	}

	locations, err := t.LocationStore.FindByUserIdAndDeviceIdAndBetweenCreatedOn(db, user.ID, deviceId, input.After, input.Before)

	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, echo.Map{"data": &locations})
}
