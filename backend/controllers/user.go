package controllers

import (
	"locator/models"
	"locator/utils"
	"net/http"

	"github.com/labstack/echo/v4"
)

// UserController represents users controller.
type UserController struct {
	UserStore models.IUserStore
}

// FindAll returns all users as a PageableResponse.
func (t *UserController) FindAll(c echo.Context) error {
	db := utils.GetDB(c)
	pageable := utils.GetPageable(c)

	users, totalElements, err := t.UserStore.FindAll(db, pageable.Page, pageable.Size)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, &models.PageableResponse{
		Page:          pageable.Page,
		Size:          pageable.Size,
		TotalElements: totalElements,
		Data:          &users,
	})
}

// FindAll returns user associated with passed id.
func (t *UserController) Get(c echo.Context) error {
	db := utils.GetDB(c)
	id, err := utils.GetIdParam(c)
	if err != nil {
		return err
	}

	user, err := t.UserStore.GetById(db, id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, user)
}

// Delete deletes user associated with passed id.
func (t *UserController) Delete(c echo.Context) error {
	db := utils.GetDB(c)
	id, err := utils.GetIdParam(c)
	if err != nil {
		return err
	}

	err = t.UserStore.DeleteById(db, id)
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}

// GetLoggedUser deletes user associated with passed id.
func GetLoggedUser(c echo.Context) error {
	user := utils.GetUser(c)

	return c.JSON(http.StatusOK, user)
}
