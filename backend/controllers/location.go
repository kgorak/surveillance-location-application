package controllers

import (
	"locator/models"
	"locator/utils"
	"net/http"

	"github.com/labstack/echo/v4"
)

// LocationController represents locations controller.
type LocationController struct {
	LocationStore models.ILocationStore
}

// FindAll returns all locations as a PageableResponse.
func (t *LocationController) FindAll(c echo.Context) error {
	db := utils.GetDB(c)
	pageable := utils.GetPageable(c)

	locations, totalElements, err := t.LocationStore.FindAll(db, pageable.Page, pageable.Size)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, models.PageableResponse{
		Page:          pageable.Page,
		Size:          pageable.Size,
		TotalElements: totalElements,
		Data:          locations,
	})
}
