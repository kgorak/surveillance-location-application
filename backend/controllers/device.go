package controllers

import (
	"locator/models"
	"locator/utils"
	"net/http"

	"github.com/labstack/echo/v4"
)

// DeviceController represents devices controller.
type DeviceController struct {
	DeviceStore models.IDeviceStore
}

// FindAll returns all devices as a PageableResponse.
func (t *DeviceController) FindAll(c echo.Context) error {
	db := utils.GetDB(c)
	pageable := utils.GetPageable(c)

	devices, totalElements, err := t.DeviceStore.FindAll(db, pageable.Page, pageable.Size)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, &models.PageableResponse{
		Page:          pageable.Page,
		Size:          pageable.Size,
		TotalElements: totalElements,
		Data:          &devices,
	})
}

// Get returns a device by its id.
func (t *DeviceController) Get(c echo.Context) error {
	db := utils.GetDB(c)
	id, err := utils.GetIdParam(c)
	if err != nil {
		return err
	}

	device, err := t.DeviceStore.GetById(db, id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, &device)
}

// Delete removes a device by its id.
func (t *DeviceController) Delete(c echo.Context) error {
	db := utils.GetDB(c)
	id, err := utils.GetIdParam(c)
	if err != nil {
		return err
	}

	if err := t.DeviceStore.DeleteById(db, id); err != nil {
		return err
	}

	return c.NoContent(http.StatusNoContent)
}
