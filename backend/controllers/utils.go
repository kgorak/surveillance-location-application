package controllers

import (
	"github.com/labstack/echo/v4"
)

// BindAndValidate tries to bind request payload to passed struct and validate it.
func BindAndValidate(c echo.Context, obj interface{}) error {
	if err := c.Bind(obj); err != nil {
		return err
	}

	if err := c.Validate(obj); err != nil {
		return err
	}

	return nil
}
