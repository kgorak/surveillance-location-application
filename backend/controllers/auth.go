// controllers contains controllers used in the application.
package controllers

import (
	"locator/models"
	"locator/utils"
	"locator/utils/tokens"
	"net/http"

	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
)

// AuthController represents authentication controller.
type AuthController struct {
	UserStore models.IUserStore
}

// SignUp allows user to sign up.
func (t *AuthController) SignUp(c echo.Context) error {
	db := utils.GetDB(c)

	var input models.SignUpForm
	if err := BindAndValidate(c, &input); err != nil {
		return err
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(input.Password), 10)
	if err != nil {
		return err
	}

	user, err := t.UserStore.Create(db, input.Email, string(hashedPassword))
	if err != nil {
		return echo.NewHTTPError(http.StatusConflict, "User already exists").SetInternal(err)
	}

	token, err := tokens.CreateToken(user.Email)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, echo.Map{"token": token})
}

// SignIn allows user to sign in.
func (t *AuthController) SignIn(c echo.Context) error {
	db := utils.GetDB(c)

	var input models.SignInForm
	if err := BindAndValidate(c, &input); err != nil {
		return err
	}

	user, err := t.UserStore.GetByEmail(db, input.Email)

	errForbidden := *echo.ErrForbidden

	if err != nil {
		return errForbidden.SetInternal(err)
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(input.Password)); err != nil {
		return errForbidden.SetInternal(err)
	}

	token, err := tokens.CreateToken(user.Email)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, echo.Map{"token": token})
}
