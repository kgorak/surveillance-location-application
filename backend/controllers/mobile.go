package controllers

import (
	"locator/models"
	"locator/utils"
	r "locator/utils/response"
	"net/http"

	"github.com/labstack/echo/v4"
)

// MobileController represents controller used by mobile app.
type MobileController struct {
	LocationStore models.ILocationStore
}

// GetLoggedDevice returns device associated with the provided token.
func (t *MobileController) GetLoggedDevice(c echo.Context) error {
	device := utils.GetDevice(c)

	return c.JSON(http.StatusOK, device)
}

// SaveLocation persists sent device location.
func (t *MobileController) SaveLocation(c echo.Context) error {
	device := utils.GetDevice(c)
	db := utils.GetDB(c)

	var input models.CreateLocationForm
	if err := BindAndValidate(c, &input); err != nil {
		return c.JSON(http.StatusUnprocessableEntity, r.Error(err))
	}

	location, err := t.LocationStore.Create(db, device.ID, *input.Lat, *input.Long)

	if err != nil {
		return c.JSON(http.StatusInternalServerError, r.Error(err))
	}

	return c.JSON(http.StatusOK, &location)
}
