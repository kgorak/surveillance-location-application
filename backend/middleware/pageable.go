package middleware

import (
	co "locator/controllers"
	"locator/models"

	"github.com/labstack/echo/v4"
)

// Pageable extracts page information from query string.
func Pageable(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var pageable models.PageableInput
		var page uint = 0
		var size uint = 20

		if err := co.BindAndValidate(c, &pageable); err == nil {
			if pageable.Page != nil {
				page = *pageable.Page
			}
			if pageable.Size != nil && *pageable.Size > 0 {
				size = *pageable.Size
			}
		}
		c.Set("pageable", &models.Pageable{Page: page, Size: size})

		return next(c)
	}
}
