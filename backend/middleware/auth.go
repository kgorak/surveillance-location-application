package middleware

import (
	"locator/models"
	"locator/utils"
	r "locator/utils/response"
	"locator/utils/tokens"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
)

// HasRole checks if user has a required role.
func HasRole(role string) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			user := utils.GetUser(c)

			if user.Email != "admin@locator.com" {
				return echo.ErrForbidden
			}

			return next(c)
		}
	}
}

// UserAuthorize searches user based on passed Authorization header.
func UserAuthorize(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		tokenString, ok := extractTokenString(c)
		if !ok {
			return c.JSON(http.StatusUnauthorized, r.AuthorizationRequired)
		}

		token, err := tokens.Parse(tokenString)
		if err != nil {
			return c.JSON(http.StatusUnauthorized, r.MalformedToken)
		}

		if !token.Valid {
			return c.JSON(http.StatusUnauthorized, r.InvalidToken)
		}

		subject := token.Claims.(jwt.MapClaims)["sub"].(string)
		db := utils.GetDB(c)
		var user models.User
		if err := db.Where(&models.User{Email: subject}).First(&user).Error; err != nil {
			return c.JSON(http.StatusForbidden, r.InvalidToken)
		}

		c.Set("user", &user)

		return next(c)
	}
}

// DeviceAuthorize searches device based on passed Authorization header.
func DeviceAuthorize(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		tokenString, ok := extractTokenString(c)

		if !ok {
			return c.JSON(http.StatusUnauthorized, r.AuthorizationRequired)
		}

		db := utils.GetDB(c)

		var device models.Device
		if err := db.Where(&models.Device{APIKey: tokenString}).First(&device).Error; err != nil {
			if gorm.IsRecordNotFoundError(err) {
				return c.JSON(http.StatusForbidden, "Token is not usable")
			} else {
				return c.JSON(http.StatusInternalServerError, r.Error(err))
			}
		}

		c.Set("device", &device)
		return next(c)
	}
}

// extractTokenString extracts token from Authoriazation header.
func extractTokenString(c echo.Context) (string, bool) {
	header := c.Request().Header.Get("Authorization")
	prefix := "Bearer "

	if !strings.HasPrefix(header, prefix) {
		return "", false
	}

	return strings.TrimPrefix(header, prefix), true
}
