package main

import (
	"fmt"
	"locator/controllers"
	"locator/middleware"
	"locator/models"
	r "locator/utils/response"
	"net/http"
	"os"
	"reflect"

	"github.com/go-playground/validator/v10"
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	middlewares "github.com/labstack/echo/v4/middleware"
	"github.com/spf13/viper"
)

// CustomValidator represents object that can validate user inputs.
type CustomValidator struct {
	Validator *validator.Validate
}

// Validate validates passed object.
func (t *CustomValidator) Validate(obj interface{}) error {
	return t.Validator.Struct(obj)
}

func main() {

	isAdmin := middleware.HasRole("ADMIN")

	engine := echo.New()

	engine.Validator = &CustomValidator{Validator: validator.New()}

	engine.Use(middlewares.Logger())
	engine.Use(middlewares.Recover())
	engine.Use(middlewares.GzipWithConfig(middlewares.GzipConfig{
		Level: 6,
	}))

	engine.HTTPErrorHandler = func(err error, c echo.Context) {
		fmt.Println("HTTPErrorHandler", reflect.TypeOf(err), err.Error())
		var responseError error
		defer func() {
			if responseError != nil {
				fmt.Println("Error occurred during handling error", err.Error())
			}
		}()

		if gorm.IsRecordNotFoundError(err) {
			responseError = c.JSON(http.StatusNotFound, r.Error(err))
		} else if httpError, ok := err.(*echo.HTTPError); ok {
			if httpError.Internal != nil {
				fmt.Println(httpError.Internal.Error())
			}
			responseError = c.JSON(httpError.Code, r.ErrorWithText(fmt.Sprintf("%v", httpError.Message)))
		} else if validationErrors, ok := err.(validator.ValidationErrors); ok {
			responseError = c.JSON(http.StatusUnprocessableEntity, r.Error(validationErrors))
		} else {
			responseError = c.JSON(http.StatusInternalServerError, r.Error(err))
		}
	}

	info, err := os.Stat("./dist")
	if !os.IsNotExist(err) && info.IsDir() {
		fmt.Println("Using dist folder as static assets")
		engine.Static("/", "dist")
	} else {
		fmt.Println("Dist folder doesn't exist")
	}

	locationStore := &models.DefaultLocationStore{}
	deviceStore := &models.DefaultDeviceStore{}
	userStore := &models.DefaultUserStore{}

	db := models.SetupModels()

	engine.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			c.Set("db", db)
			return next(c)
		}
	})

	r := engine.Group("/api")

	rPublic := r.Group("/public")
	{
		auth := &controllers.AuthController{UserStore: userStore}
		rPublic.POST("/auth/sign-in", auth.SignIn)
		rPublic.POST("/auth/sign-up", auth.SignUp)
	}

	rPrivate := r.Group("/private", middleware.UserAuthorize)
	{
		rUser := rPrivate.Group("/user")
		{
			rMe := rUser.Group("/me")
			{
				rMe.GET("", controllers.GetLoggedUser)
				// TODO change password etc
			}
		}

		userDevice := &controllers.UserDeviceController{
			DeviceStore:   deviceStore,
			LocationStore: locationStore,
		}
		rDevice := rPrivate.Group("/device")
		{
			rDevice.GET("", userDevice.FindAll, middleware.Pageable)
			rDevice.POST("", userDevice.Create)
			rDevice.GET("/:id", userDevice.Get)
			rDevice.DELETE("/:id", userDevice.Delete)
			rDevice.GET("/:id/location", userDevice.FindAllLocations)
		}

		rLocation := rPrivate.Group("/location")
		{
			location := &controllers.LocationController{LocationStore: locationStore}
			rLocation.GET("", location.FindAll, middleware.Pageable)
		}
	}

	rManagement := r.Group("/management", middleware.UserAuthorize, isAdmin)
	{
		cUser := &controllers.UserController{UserStore: userStore}
		rUser := rManagement.Group("/user")
		{
			rUser.GET("", cUser.FindAll, middleware.Pageable) // Find all
			// rUser.POST("", cUser.Create) // Create
			rUser.GET("/:id", cUser.Get) // Get
			// rUser.POST("/:id")   // Update
			// rUser.PUT("/:id")    // Update
			// rUser.PATCH("/:id")  // Update partial
			rUser.DELETE("/:id", cUser.Delete) // Delete
		}

		cDevice := &controllers.DeviceController{DeviceStore: deviceStore}
		rDevice := rManagement.Group("/device")
		{
			rDevice.GET("", cDevice.FindAll, middleware.Pageable) // Find all
			// rDevice.POST("", cDevice)       // Create
			rDevice.GET("/:id", cDevice.Get) // Get
			// rDevice.POST("/:id", cDevice)   // Update
			// rDevice.PUT("/:id", cDevice)    // Update
			// rDevice.PATCH("/:id", cDevice)  // Update partial
			rDevice.DELETE("/:id", cDevice.Delete) // Delete
		}
	}

	cMobile := &controllers.MobileController{LocationStore: locationStore}
	rMobile := r.Group("/mobile", middleware.DeviceAuthorize)
	{
		rMobile.GET("/me", cMobile.GetLoggedDevice)
		rMobile.POST("/location", cMobile.SaveLocation)
	}

	viper.SetDefault("PORT", 3000)
	port := viper.Get("PORT")

	addr := fmt.Sprintf(":%v", port)
	fmt.Println("Listening on address", addr)

	engine.Start(addr)
}
