package models

import (
	"github.com/jinzhu/gorm"
)

type Device struct {
	ID     uint   `json:"id" gorm:"primary_key"`
	Name   string `json:"name" gorm:"not null"`
	UserID uint   `json:"userId" gorm:"not null"`
	APIKey string `json:"key" gorm:"unique;not null"` // TODO: change with JWT token
}

type CreateDeviceForm struct {
	Name *string `json:"name"`
}

type UpdateDeviceForm struct {
	Name string `json:"name" validate:"required"`
}

type IDeviceStore interface {
	GetById(db *gorm.DB, id uint) (*Device, error)
	FindAll(db *gorm.DB, page uint, size uint) ([]Device, uint, error)
	FindByUserId(db *gorm.DB, userId uint, page uint, size uint) ([]Device, uint, error)
	Create(db *gorm.DB, userId uint, name string, apiKey string) (device *Device, err error)
	DeleteById(db *gorm.DB, id uint) error
	DeleteByIdAndUserId(db *gorm.DB, id uint, userId uint) error
}

var _ IDeviceStore = &DefaultDeviceStore{}

type DefaultDeviceStore struct{}

func (t *DefaultDeviceStore) GetById(db *gorm.DB, id uint) (*Device, error) {
	device := Device{}

	result := db.First(&device, id)

	if result.Error != nil {
		return nil, result.Error
	}
	return &device, nil
}

func (t *DefaultDeviceStore) FindAll(db *gorm.DB, page uint, size uint) ([]Device, uint, error) {
	var devices []Device
	var totalElements uint

	result := db.Model(devices).Count(&totalElements).Limit(size).Offset(page * size).Find(&devices)

	if result.Error != nil {
		return nil, 0, result.Error
	}
	return devices, totalElements, nil
}

func (t *DefaultDeviceStore) FindByUserId(db *gorm.DB, userId uint, page uint, size uint) ([]Device, uint, error) {
	var devices []Device
	var totalElements uint

	result := db.Model(devices).Where("user_id = ?", userId).
		Count(&totalElements).
		Limit(size).Offset(page * size).Find(&devices)

	if result.Error != nil {
		return nil, 0, result.Error
	}
	return devices, totalElements, nil
}

func (t *DefaultDeviceStore) Create(db *gorm.DB, userId uint, name string, apiKey string) (*Device, error) {
	device := Device{
		Name:   name,
		UserID: userId,
		APIKey: apiKey,
	}

	if err := db.Create(&device).Error; err != nil {
		return nil, err
	}

	return &device, nil
}

func (t *DefaultDeviceStore) DeleteById(db *gorm.DB, id uint) error {
	result := db.Where(Device{ID: id}).Delete(Device{})

	return handleRowsAffected(result)
}

func (t *DefaultDeviceStore) DeleteByIdAndUserId(db *gorm.DB, id uint, userId uint) error {
	result := db.Where(Device{ID: id, UserID: userId}).Delete(Device{})

	return handleRowsAffected(result)
}
