package models

import "github.com/jinzhu/gorm"

func handleRowsAffected(result *gorm.DB) error {
	if result.Error != nil {
		return result.Error
	} else if result.RowsAffected <= 0 {
		return gorm.ErrRecordNotFound
	}
	return nil
}
