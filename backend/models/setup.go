package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/spf13/viper"
	"golang.org/x/crypto/bcrypt"
)

func SetupModels() *gorm.DB {
	viper.SetDefault("ECHO_MODE", "development")
	// viper.AddConfigPath(".")
	viper.SetConfigFile(".env")
	if err := viper.ReadInConfig(); err != nil {
		panic("cannot read config")
	}
	viper.AutomaticEnv()

	var postgres_connname string

	if viper.IsSet("DATABASE_URL") {
		// heroku
		postgres_connname = viper.GetString("DATABASE_URL")
	} else {
		viper_user := viper.Get("POSTGRES_USER")
		viper_password := viper.Get("POSTGRES_PASSWORD")
		viper_db := viper.Get("POSTGRES_DB")
		viper_host := viper.Get("POSTGRES_HOST")
		viper_port := viper.Get("POSTGRES_PORT")

		postgres_connname = fmt.Sprintf("host=%v port=%v user=%v dbname=%v password=%v sslmode=disable", viper_host, viper_port, viper_user, viper_db, viper_password)
	}

	fmt.Println("conname is\t\t", postgres_connname)

	db, err := gorm.Open("postgres", postgres_connname)
	if err != nil {
		panic("Failed to connect to database!")
	}

	db.AutoMigrate(&User{})
	db.AutoMigrate(&Device{})
	db.AutoMigrate(&Location{})

	isDev := viper.GetString("ECHO_MODE") != "production"
	if isDev {
		var user User
		err := db.Where("email = ?", "admin@locator.com").First(&user).Error
		if gorm.IsRecordNotFoundError(err) {
			hashedPassword, _ := bcrypt.GenerateFromPassword([]byte("password"), 10)
			admin := User{Email: "admin@locator.com", Password: string(hashedPassword)}
			db.Create(&admin)
		}
	}

	return db
}
