package models

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	ID       uint     `json:"id" gorm:"primary_key"`
	Email    string   `json:"email" gorm:"unique;not null"`
	Password string   `json:"-" gorm:"not null"`
	Devices  []Device `json:"-"`
}

type SignUpForm struct {
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type SignInForm struct {
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
}

type IUserStore interface {
	FindAll(db *gorm.DB, page uint, size uint) (users []User, totalElements uint, err error)
	GetById(db *gorm.DB, id uint) (*User, error)
	GetByEmail(db *gorm.DB, email string) (*User, error)
	Create(db *gorm.DB, email string, passwordHash string) (*User, error)
	DeleteById(db *gorm.DB, id uint) error
}

type DefaultUserStore struct{}

var _ IUserStore = &DefaultUserStore{}

func (t *DefaultUserStore) FindAll(db *gorm.DB, page uint, size uint) ([]User, uint, error) {
	var totalElements uint
	var users []User
	result := db.Model(users).Count(&totalElements).Limit(size).Offset(page * size).Find(&users)
	if result.Error != nil {
		return nil, 0, result.Error
	}

	return users, totalElements, nil
}

func (t *DefaultUserStore) GetById(db *gorm.DB, id uint) (*User, error) {
	var user User
	if err := db.First(&user, id).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (t *DefaultUserStore) GetByEmail(db *gorm.DB, email string) (*User, error) {
	var user User
	if err := db.First(&user, &User{Email: email}).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (t *DefaultUserStore) Create(db *gorm.DB, email string, passwordHash string) (*User, error) {
	user := User{Email: email, Password: passwordHash}
	if err := db.Create(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (t *DefaultUserStore) DeleteById(db *gorm.DB, id uint) error {
	if err := db.Delete(&User{ID: id}).Error; err != nil {
		return err
	}
	return nil
}
