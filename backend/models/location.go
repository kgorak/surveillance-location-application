package models

import (
	"time"

	"github.com/jinzhu/gorm"
)

type Location struct {
	ID        uint      `json:"id" gorm:"primary_key"`
	Lat       float64   `json:"lat" gorm:"not null"`
	Long      float64   `json:"long" gorm:"not null"`
	CreatedOn time.Time `json:"createdOn" gorm:"not null"`

	DeviceID uint `json:"deviceId" gorm:"not null"`
}

type QueryLocationForm struct {
	Before time.Time `json:"before" query:"before" validate:"required"`
	After  time.Time `json:"after" query:"after" validate:"required"`
}

type CreateLocationForm struct {
	Lat  *float64 `json:"lat" validate:"required"`
	Long *float64 `json:"long" validate:"required"`
}

type ILocationStore interface {
	FindAll(db *gorm.DB, page uint, size uint) (locations []Location, totalElements uint, err error)
	FindByUserIdAndDeviceIdAndBetweenCreatedOn(db *gorm.DB, userId uint, deviceId uint, after time.Time, before time.Time) ([]Location, error)
	Create(db *gorm.DB, deviceId uint, lat float64, long float64) (*Location, error)
}

var _ ILocationStore = &DefaultLocationStore{}

type DefaultLocationStore struct{}

func (t *DefaultLocationStore) FindAll(db *gorm.DB, page uint, size uint) ([]Location, uint, error) {
	var locations []Location
	var totalElements uint

	query := db.Model(locations).Count(&totalElements).Limit(size).Offset(page * size).Find(&locations)
	if err := query.Error; err != nil {
		return nil, 0, err
	}

	return locations, totalElements, nil
}

func (t *DefaultLocationStore) FindByUserIdAndDeviceIdAndBetweenCreatedOn(db *gorm.DB, userId uint, deviceId uint, after time.Time, before time.Time) ([]Location, error) {
	var locations []Location
	query := db.
		Joins("JOIN devices ON devices.id = locations.device_id AND devices.id = ?", deviceId).
		Joins("JOIN users ON users.id = devices.user_id").
		Where("users.id = ? AND locations.created_on BETWEEN ? AND ?", userId, after, before).
		Find(&locations)

	if query.Error != nil {
		return nil, query.Error
	}

	return locations, nil
}

func (t *DefaultLocationStore) Create(db *gorm.DB, deviceId uint, lat float64, long float64) (*Location, error) {
	location := Location{
		Lat:       lat,
		Long:      long,
		CreatedOn: time.Now(),
		DeviceID:  deviceId,
	}

	if err := db.Create(&location).Error; err != nil {
		return nil, err
	}

	return &location, nil
}
