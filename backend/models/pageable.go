package models

type PageableInput struct {
	Page *uint `json:"page" query:"page"`
	Size *uint `json:"size" query:"size"`
}

type Pageable struct {
	Page uint
	Size uint
}

type PageableResponse struct {
	Page          uint        `json:"page"`
	Size          uint        `json:"size"`
	TotalElements uint        `json:"totalElements"`
	Data          interface{} `json:"data"`
}
