package response

import (
	"fmt"

	"github.com/labstack/echo/v4"
)

var BadCredentials = ErrorWithText("Bad credentials")
var InvalidId = InvalidParameter("id")
var AuthorizationRequired = ErrorWithText("Authorization required")
var MalformedToken = ErrorWithText("Malformed token")
var InvalidToken = ErrorWithText("Invalid token")

func InvalidParameter(parameterName string) echo.Map {
	return echo.Map{"error": fmt.Sprintf("Invalid '%v' parameter", parameterName)}
}

func Error(err error) echo.Map {
	return ErrorWithText(err.Error())
}

func ErrorWithText(text string) echo.Map {
	return echo.Map{"error": text}
}
