package utils

import (
	"locator/models"
	"strconv"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
)

func GetIdParam(c echo.Context) (uint, error) {
	id, err := strconv.ParseUint(c.Param("id"), 10, 32)
	if err != nil {
		return 0, err
	}

	return uint(id), nil
}

func GetDB(c echo.Context) *gorm.DB {
	return c.Get("db").(*gorm.DB)
}

func GetPageable(c echo.Context) *models.Pageable {
	return c.Get("pageable").(*models.Pageable)
}

func GetUser(c echo.Context) *models.User {
	return c.Get("user").(*models.User)
}

func GetDevice(c echo.Context) *models.Device {
	return c.Get("device").(*models.Device)
}
