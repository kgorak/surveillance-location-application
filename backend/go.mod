module locator

go 1.14

// +heroku goVersion 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.3.0
	github.com/jinzhu/gorm v1.9.12
	github.com/labstack/echo/v4 v4.1.16
	github.com/robertkrimen/godocdown v0.0.0-20130622164427-0bfa04905481 // indirect
	github.com/spf13/viper v1.7.0
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d
	google.golang.org/genproto v0.0.0-20191108220845-16a3f7862a1a
)
