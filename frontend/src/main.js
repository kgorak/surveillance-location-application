import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store from "./store"
import axios from "axios"
import { BootstrapVue, IconsPlugin, BIcon } from "bootstrap-vue"
import VueAxios from "vue-axios"
import { LMap, LTileLayer, LMarker, LPolyline, LPopup } from "vue2-leaflet"
import "leaflet/dist/leaflet.css"

import "bootstrap/dist/css/bootstrap.css"
import "bootstrap-vue/dist/bootstrap-vue.css"

Vue.component("l-map", LMap)
Vue.component("l-tile-layer", LTileLayer)
Vue.component("l-marker", LMarker)
Vue.component("l-polyline", LPolyline)
Vue.component("l-popup", LPopup)

// Fix marker icons
import { Icon } from "leaflet"

delete Icon.Default.prototype._getIconUrl
Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
  iconUrl: require("leaflet/dist/images/marker-icon.png"),
  shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
})

import { library } from "@fortawesome/fontawesome-svg-core"
import { faQrcode } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"

library.add(faQrcode)
Vue.component("font-awesome-icon", FontAwesomeIcon)

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.component("BIcon", BIcon)

export const httpClient = axios.create({
  baseURL: "/api",
})
httpClient.interceptors.request.use(config => {
  const token = store.getters["auth/token"]
  if (!token) {
    router.push("/sign-in")
    return Promise.reject("Authorization required")
  }

  config.headers = {
    ...config.headers,
    Authorization: `Bearer ${token}`,
  }
  return config
})

httpClient.interceptors.response.use(
  i => i,
  error => {
    if (error.response?.status === 401) {
      const routePath = router.currentRoute?.path
      const encodedUrl = routePath ? `/sign-in?return_url=${encodeURIComponent(routePath)}` : `/sign-in`
      router.push(encodedUrl)

      return Promise.reject("Token expired")
    }

    return Promise.reject(error)
  },
)

Vue.use(VueAxios, httpClient)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount("#app")
