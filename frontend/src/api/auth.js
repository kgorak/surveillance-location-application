import axios from "axios"

import { httpClient } from "@/main"

export async function SignIn(email, password) {
  return await axios.post("/api/public/auth/sign-in", {
    email,
    password,
  })
}

export async function SignUp(email, password) {
  return await axios.post("/api/public/auth/sign-up", {
    email,
    password,
  })
}

export async function GetLoggedUserDetails() {
  return await httpClient.get("/private/user/me")
}
