import { httpClient } from "@/main"

export async function FetchAll(page = 0, size = 20) {
  return await httpClient.get(`/private/device`, {
    params: {
      page,
      size,
    },
  })
}

export async function GetById(id) {
  if (!id) return Promise.reject("id is required")

  return await httpClient.get(`/private/device/${id}`)
}

export async function Create(name) {
  const data = { ...(name && { name }) }
  return await httpClient.post(`private/device`, data)
}

export async function Update(id, name) {
  if (!id) return Promise.reject("id is required")
  if (!name) return Promise.reject("name is required")

  return await httpClient.post(`private/device/${id}`, {
    name,
  })
}

export async function Delete(id) {
  if (!id) return Promise.reject("id is required")

  return await httpClient.delete(`/private/device/${id}`)
}
