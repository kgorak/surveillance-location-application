import moment from "moment"
import { httpClient } from "@/main"

export async function FetchAll(id = 0, after, before) {
  after = after ? moment(after) : moment()
  before = before ? moment(before) : moment()

  return await httpClient.get(`/private/device/${id}/location`, {
    params: {
      after: after.startOf("day").format(),
      before: before.endOf("day").format(),
    },
  })
}
