// import * as AuthService from "@/api/auth"
import router from "@/router"

const state = {
  token: "",
}
const getters = {
  token(state) {
    return state.token
  },
}
const actions = {
  signOut(context) {
    context.commit("clearToken")

    router.push("/")
  },
}
const mutations = {
  clearToken(state) {
    state.token = ""
  },
  setToken(state, token) {
    state.token = token
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
