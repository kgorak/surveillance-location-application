export const delay = (fn, timeout) =>
  new Promise(resolve => {
    setTimeout(async () => {
      const result = await fn()
      resolve(result)
    }, timeout)
  })

export const executeAtLeast = async (fn, timeout = 800) => {
  const [result] = await Promise.all([
    fn,
    new Promise(resolve => {
      setTimeout(resolve, timeout)
    }),
  ])
  return result
}

export const delayBy = (timeout = 800) => () => delay(() => {}, timeout)
