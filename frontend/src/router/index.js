import Vue from "vue"
import VueRouter from "vue-router"
import Home from "../views/Home.vue"
import SignIn from "../views/SignIn.vue"
import SignUp from "../views/SignUp.vue"
import Devices from "../views/Devices.vue"
import UserList from "@/components/UserList"
import UserForm from "@/components/UserForm.vue"
import Users from "../views/Users.vue"
import Map from "../views/Map.vue"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/sign-in",
    name: "SignIn",
    alias: "/login",
    component: SignIn,
  },
  {
    path: "/sign-up",
    name: "SignUp",
    component: SignUp,
  },
  {
    path: "/devices/:id/map",
    name: "Map",
    component: Map,
    props: route => {
      let id = route.params.id
      if (typeof id === "string") {
        if (!(id = Number(id))) {
          throw new Error("Invalid parameter")
        }
      }
      return { id }
    },
  },
  {
    path: "/devices",
    name: "Devices",
    component: Devices,
  },
  {
    path: "/users",
    component: Users,
    children: [
      { path: "", component: UserList },
      { path: "form", component: UserForm },
    ],
  },
]

const router = new VueRouter({
  routes,
})

export default router
