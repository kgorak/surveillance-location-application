const MomentLocalesPlugin = require('moment-locales-webpack-plugin');

exports.devServer = {
  proxy: 'http://localhost:3000',
}
exports.runtimeCompiler = true
exports.configureWebpack = {
  plugins: [
    new MomentLocalesPlugin({
      localesToKeep: ["pl"],
    }),
  ],
}
exports.chainWebpack = config => config.module
  .rule('vue')
  .use('vue-loader')
  .loader('vue-loader')
  .tap(args => {
    args.compilerOptions.whitespace = 'condense'
  })
